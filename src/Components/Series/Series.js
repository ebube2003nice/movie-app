import React, { Component } from 'react';
import 'whatwg-fetch';
import SeriesList from '../SeriesList/SeriesList';
import Loader from '../Loader/Loader'
import './Series.css';
import './Form.css';
// import PopularMovies from '../PopularMovies/PopularMovies';
class Series extends Component {
    // State
    state = { 
        series:[],
        seriesName:"",
        isFetching :false
        

     }
     

     //EVENTS
     InputHandler=(e)=>{
         this.setState({seriesName:e.target.value,isFetching:true})
        fetch(`http://api.tvmaze.com/search/shows?q=${e.target.value}`)
        .then((resposne)=>resposne.json())
        .then(data=>this.setState({
            series:data,
          isFetching:false
            
        }))
     }
    render() { 
        const{seriesName,isFetching,series}=this.state
        console.log(series)
        return (
            <div>
                <p>List of movies in  array -{this.state.series.length}</p>
                <div className="form">
                <input value={seriesName} type="text" onChange={this.InputHandler}/>
                </div>
               



                    {!isFetching && series.length=== 0 && seriesName.trim()=== "" &&
                    
                    <p style={{color:"white"}}>Please enter Movie name</p>
                    // <PopularMovies />
                    }


                    {!isFetching && series.length=== 0 && seriesName.trim()!== "" &&
                   <p style={{color:"white"}}>Series name not found in our database </p>
                    }

                        {
                            isFetching && <Loader />
                        }



                    {!isFetching &&
                    
                    <ul className="seriesContent">
                    {this.state.series.map(list=>(
                        <SeriesList name={list.show.name} series={list} key={list.show.id} genre={list.show.genres}    />
                    ))}
                </ul>
                    }
                
            </div>
          );
    }
}
 
export default Series;