import React, { Component } from 'react';
import './SeriesPage.css';
import Loader from '../Loader/Loader'
class SeriesPage extends Component {
    state = { 
        show:null
     }

     componentDidMount(){
        const {id} = this.props.match.params
         fetch(` http://api.tvmaze.com/shows/${id}?embed=episodes`)
         .then((response)=>response.json())
         .then((information)=>
      
        this.setState({
            show:information
        }))
     }




    render() { 
       const{show}=this.state
       console.log(show)

       
        return (
            <div className="container">
                {show === null && 
              <Loader />
                }


               {show !== null &&
               <div className="seriesContainer">
               <h1>{show.name}</h1>

               <div className="subinfo">
               <img src={show.image.medium} alt="show"/>
               <p>Name : <span>{show.name}</span></p>
               <p>Genre : <span>{show.genres}</span></p>
               <p>language: <span>{show.language}</span></p>
               <p>Premiered: <span>{show.premiered}</span></p>
               <p>Status: <span>{show.status}</span></p>
               <p>Duration: <span>{show.runtime} mins</span></p>
               {/* <p>Network: <span>{show.network.name}</span></p>
               <p>Country of production: <span>{show.network.country.name}</span></p> */}
               <p>Rating: <span>{show.rating.average}/10</span></p>
               </div>

               <p><span>{show.summary}</span></p>
               </div>
               }
            </div>
          );
    }
}
 
export default SeriesPage;