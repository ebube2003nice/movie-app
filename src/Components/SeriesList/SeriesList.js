import React from 'react';
import './SeriesList.module.css';
import {Link} from 'react-router-dom'
const SeriesList = ({name,series,genre}) => {
     
     
    return ( 
         <div>
        <Link to={`/series/${series.show.id}`}>
        <h3>{name}</h3>
        </Link>
             <p className="genre">{genre}</p>
        </div>
      
        
     );
}
 
export default SeriesList;