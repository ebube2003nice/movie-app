import React from 'react';
const Information=({name,image})=>{
    return(
        <div className="information">
            <img src={image} alt="movie" />
            <h4>{name}</h4>
        </div>
        
    )

}
 
export default Information;