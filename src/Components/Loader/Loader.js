import React from 'react';
import Loadersrc from '../../assets/Pulse-1s-200px.gif'
const Loader = () => {
    return ( 
        <div>
            
            <img
            style={{width:75}}
             alt="loader" src={Loadersrc}></img>
        </div>
     );
}
 
export default Loader;