import React from 'react';
import Series from '../Series/Series';
import {Switch,Route} from "react-router-dom"
import SeriesPage from '../SeriesList/Seriespage'


const Main = (props) => {
    return ( 
        <Switch>
       <Route exact path ="/" component={Series}></Route>
       <Route exact path ="/series/:id" component={SeriesPage}></Route>
     
        </Switch>


     );
}
 
export default Main;